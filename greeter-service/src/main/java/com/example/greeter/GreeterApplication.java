package com.example.greeter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.sleuth.Sampler;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class GreeterApplication {
    private final static Logger log = LoggerFactory.getLogger(GreeterApplication.class);

    private final String greeting;

    public GreeterApplication(
        @Value("${greeter-service.greeting}") final String greeting) {
        this.greeting = greeting;
    }

    @RequestMapping("/")
    public String home() {
        log.info("Returning greeting {}", greeting);
        return greeting;
    }

    public static void main(String[] args) {
        SpringApplication.run(GreeterApplication.class, args);
    }

    @Bean
    @ConditionalOnProperty(value = "zipkin.sample.always", matchIfMissing = false)
    public Sampler defaultSampler() {
        return new AlwaysSampler();
    }
}
