#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

status=$(minikube status | grep -v Running)
if [[ "x$status" != "x" ]]; then
	echo "It does not seem like minikube is running?"
	echo "Start it with something like"
	echo "  minikube start [--vm-driver xhyve]"
	exit 1
fi

eval $(minikube docker-env)

# cd $DIR/..
# mvn clean package

kubectl config use-context minikube

kubectl apply -f $DIR/zipkin-inmem.yaml

echo "Zipkin url:"
minikube service zipkin --url

kubectl delete service greeter-service
kubectl delete deployment greeter-service
kubectl delete configmap greeter-service
kubectl delete secret greeter-service

kubectl apply -f $DIR/greeter-secret.yaml
kubectl apply -f $DIR/greeter-config.yaml
kubectl apply -f $DIR/greeter-deployment.yaml
kubectl apply -f $DIR/greeter-service.yaml

echo "Greeter API url:"
minikube service greeter-service --url | head -n 1

echo
echo "Greeter Management url:"
minikube service greeter-service --url | tail -n 1
