#!/bin/bash

set -e

cd greeter-service
mvn clean package
repository=lsimons/hello
tag=`cat pom.xml | egrep -o '<version>[^<]+</version>' | head -n 1 |\
    sed 's|<version>||' | sed 's|</version>||'`

docker push $repository:$tag
